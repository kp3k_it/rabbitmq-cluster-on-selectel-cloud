#!/usr/bin/env python
import pika
import sys
message = sys.argv[1]
desthost = sys.argv[2]
connection = pika.BlockingConnection(pika.ConnectionParameters(desthost))
channel = connection.channel()
channel.queue_declare(queue='hello')
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=message+"; destination: "+desthost)
print("Message sent: %s; destination host: %s" % (message, desthost))
connection.close()
