resource "selectel_vpc_user_v2" "user_1" {
  name     = var.user_name
  password = var.user_password
}

output "user_id" {
  value = selectel_vpc_user_v2.user_1.id
}
