variable "user_password" {}

variable "keypair_name" {
  default = ""
}

variable "project_name" {
  default = "rabbitmq_project"
}

variable "user_name" {
  default = "rabbitmq_user"
}
