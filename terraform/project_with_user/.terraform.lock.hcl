# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/time" {
  version = "0.7.2"
  hashes = [
    "h1:NKy1QrNLlP5mKy5Tea6lQSRsVoyydJQKh6WvNTdBF4I=",
    "zh:0bbe0158c2a9e3f5be911b7e94477586110c51746bb13d102054f22754565bda",
    "zh:3250af7fd49b8aaf2ccc895588af05197d886e38b727e3ba33bcbb8cc96ad34d",
    "zh:35e4de0437f4fa9c1ad69aaf8136413be2369ea607d78e04bb68dc66a6a520b8",
    "zh:369756417a6272e79cad31eb2c82c202f6a4b6e4204a893f656644ba9e149fa2",
    "zh:390370f1179d89b33c3a0731691e772d5450a7d59fc66671ec625e201db74aa2",
    "zh:3d12ac905259d225c685bc42e5507ed0fbdaa5a09c30dce7c1932d908df857f7",
    "zh:75f63e5e1c68e6c5bccba4568c3564e2774eb3a7a19189eb8e2b6e0d58c8f8cc",
    "zh:7c22a2078a608e3e0278c4cbc9c483909062ebd1843bddaf8f176346c6d378b1",
    "zh:7cfb3c02f78f0060d59c757c4726ab45a962ce4a9cf4833beca704a1020785bd",
    "zh:a0325917f47c28a2ed088dedcea0d9520d91b264e63cc667fe4336ac993c0c11",
    "zh:c181551d4c0a40b52e236f1755cc340aeca0fb5dcfd08b3b1c393a7667d2f327",
  ]
}

provider "registry.terraform.io/selectel/selectel" {
  version     = "3.8.0"
  constraints = "~> 3.8.0"
  hashes = [
    "h1:8/pdkXk97TNSCZ8WU5FbMrttDWJwvmgyOctfdzsG/S8=",
    "zh:0daed553b2153816b8c401c1e253c812e00d80a2a2b88043dd776c38f19968db",
    "zh:2141a1564f7ceedd64c38de774eb9be87650642477f57dba16cafb98f28a08cb",
    "zh:22e30d100ca273904b53863c0df6d1ac971d2865563b2c2ed91f792b809f39d3",
    "zh:41645ebe8117c2318eae8bf0decb1f6ee2a43d960344d953743bc3f2cf149dbe",
    "zh:640bdcb0e0655fbdc438b31d9dfa854799672e89e8e8bee42ee4883c3e900658",
    "zh:66ea91c601406473900d91e69a7d53d14b8591fd608319dccaac49750702cc5c",
    "zh:68d99756daacf93add90d7de2456867d3b4805ba5b51e4ca94d20a1e042b62df",
    "zh:8a88f4af7183c215ad98749a0fcd4c8db6ca2eb05c1d780186159e3a1bea5749",
    "zh:a7c9b44451529ef1d7ffb882cb349798fc2809edd397c23d452ed4467d48d113",
    "zh:c89bfede0ca958f9a40731fa2550437b974338a972dd95d74fb03ef25f87d1a5",
    "zh:caab086315e48c0e4e24c120658f8a24ae4db242b67a6dfd8bb5627633e79fb6",
    "zh:ed6682b1202ae41c3f0c9e97a67900d4bf0fc356a38f2c40fa79863c54070a5c",
    "zh:f30b0c25a0af51265f4af2fb3c7a7ed70fead2d360df779ea37a4bfa4fb6b689",
    "zh:f771f00ef469c273a4acdbca0dc9aff229c3e2c13e6bb6a3d16ceed23c683abe",
  ]
}
