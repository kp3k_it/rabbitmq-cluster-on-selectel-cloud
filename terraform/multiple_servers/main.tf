resource "random_string" "random_name" {
  length  = 5
  special = false
}

resource "random_string" "random_server_name" {
  count   = var.replicas_count
  length  = 3
  special = false
}

module "flavor" {
  source               = "../flavor"
  flavor_name          = "flavor-${random_string.random_name.result}"
  flavor_vcpus         = var.server_vcpus
  flavor_ram_mb        = var.server_ram_mb
  flavor_local_disk_gb = var.server_root_disk_gb
}

module "image_datasource" {
  source     = "../image_datasource"
  image_name = var.server_image_name
}

resource "openstack_networking_port_v2" "port" {
  count      = var.replicas_count
  name       = "${var.server_name}-${random_string.random_server_name[count.index].result}-eth0"
  network_id = var.server_network_id

  fixed_ip {
    subnet_id = var.server_subnet_id
  }
}

#AKon: commented to keep quota 
#resource "openstack_networking_floatingip_v2" "floatingip_1" {
#  count = var.replicas_count
#  pool = "external-network"
#}
#
#resource "openstack_compute_floatingip_associate_v2" "cluster_ips" {
#  count       = "${var.replicas_count}"
#  floating_ip = "${element(openstack_networking_floatingip_v2.floatingip_1.*.address, count.index)}"
#  instance_id = "${element(openstack_compute_instance_v2.instance.*.id, count.index)}"
#}

resource "openstack_compute_instance_v2" "instance" {
  count             = "${var.replicas_count}"
  name              = "${var.server_name}-${random_string.random_server_name[count.index].result}"
  image_id          = module.image_datasource.image_id
  flavor_id         = module.flavor.flavor_id
  key_pair          = var.keypair_name
  availability_zone = var.server_zone

  network {
    port = openstack_networking_port_v2.port[count.index].id
  }
  
 
  lifecycle {
    ignore_changes = [image_id]
  }

  vendor_options {
    ignore_resize_confirmation = true
  }
}
