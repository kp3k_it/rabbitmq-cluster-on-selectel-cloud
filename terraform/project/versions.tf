terraform {
required_version = ">= 0.14.0"
  required_providers {
    selectel = {
      source = "selectel/selectel"
      version = "~> 3.8.0"
    }
  }
}
