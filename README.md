Intro: 

AKonovalov test task from Selectel.

Following project describes a rabbitmq cluster deployment in Selectel Cloud Provider using Terraform, Ansible and python scripts for testing.

Resources used (also present in code in modified state): 

  Terraform:
    - Selectel documentation about Selectel and Openstack providers usage: https://kb.selectel.com/docs/cloud/servers/tools/how_to_use_terraform/
    - Selectel terraform examples repository: https://github.com/selectel/terraform-examples/tree/master/modules/vpc/multiple_servers
    - Selectel habr article: https://habr.com/ru/company/selectel/blog/445162/

  Ansible: 
    - Ansible documentation: https://docs.ansible.com/ansible/latest/
    - Ansible rabbitmq role: https://github.com/IronCore864/ansible-rabbitmq-cluster

  Python:
    - RabbitMQ python tutorial: https://www.rabbitmq.com/tutorials/tutorial-one-python.html

OS: Ubuntu 20.04 server


Usage:

0. Get Selectel token, account ID; generate rsa keypair, place public key here: ~/.ssh/selectel_rsa.pub

1. Deploy infrastructure using terraform: 
- git clone https://gitlab.com/kp3k_it/rabbitmq-cluster-on-selectel-cloud.git
- cd rabbitmq-cluster-on-selectel-cloud/terraform/several_servers
- terraform init
- terraform apply -target=module.project_with_user
- terraform apply

2. Prepare VMs for cluster deployment, forward rsa keypair:
- Login to my.selectel.ru Cloud Platform, find floating ip near 'tf_server' instance;
- Login to tf_server, it will be managing server: ssh -i ~/.ssh/selectel_rsa root@$floating_ip; run "ssh-keygen" command
- generate root password in Cloud Platform console for every node except tf_server
- for every of three cluster nodes, run on tf_server with their new root passwords: "ssh-copy-id $node_ip"
- ensure you can access every node from tf_server without password
- set every cluster node name via "hostnamectl --set-hostname": rabbitmq0[123]; edit /etc/hosts on tf_server to add all three nodes with new names and their IPs.

3. Deploy ansible on tf_server and rabbitmq cluster on other nodes:

 - apt update && apt install ansible
 - git clone https://gitlab.com/kp3k_it/rabbitmq-cluster-on-selectel-cloud.git; cp -r rabbitmq-cluster-on-selectel-cloud/ansible/* /etc/ansible
 - edit /etc/ansible/playbooks/roles/ansible-rabbitmq-cluster/defaults/main.yml 'rabbitmq_hosts:' statement according IP addresses of your nodes. 
 - ansible-playbook /etc/ansible/playbooks/mq_cluster.yml

4. Test cluster:
 - ssh any cluster node: "rabbitmqctl cluster_status"; ensure there is three nodes in cluster;
 - copy rabbitmq-cluster-on-selectel-cloud/python_scripts/receive.py on any cluster node; install pika module for python; add execute permissions: chmod +x receive.py; run receive.py
 - on tf_server: copy rabbitmq-cluster-on-selectel-cloud/python_scripts/rabbitmq_send_message.py; install pika module for python; add execute permissions: chmod +x rabbitmq_send_message.py; run "rabbitmq_send_message.py yourmessage destinationhost
 - on tf_server: ssh to any cluster node with -t option: ssh rabbitmq01 -t "/root/receive.py"; you will get message 'yourmessage'

 To fix: 

  - static names of cluster nodes in terraform to exclude work with /etc/hosts
  - distribute python scripts via ansible to reduce manual operations
